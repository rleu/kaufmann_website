const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  purge: ["./_includes/**/*.html", "./_layouts/**/*.html", "./blog/*.html", "./_posts/*.html", "./*.html"],
  darkMode: false,
  theme: {
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
    },
    fontFamily: {
      "sans": ['Karla', 'Helvetica', 'Arial', 'sans-serif']
    },
    colors: {
      heroBackground: "#ffead8",
      musicBackground: "#AEB2BF",
      tourBackground: "#796C8C",
      imagesBackground: "#F2F2F2", 
      videosBackground: "#F2F2F2", 
      contactBackground: "#70748C",

      primary: "#F6F6F6", 
      secondary: "#796C8C",
      light_gray: "#6b6b6b",
      dark_gray: "#2a2a2a",
    }
  },
  corePlugins: {
    aspectRatio: false,
  },
  variants: {
    extend: {
      grayscale: ["hover", "focus"],
      margin: ["last"],
    },
    container: [],
  },
  plugins: [require("@tailwindcss/typography"), require("@tailwindcss/aspect-ratio")],
};
