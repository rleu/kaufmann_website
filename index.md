---
layout: default
hero: 
    title: Neues Album «En liaba Gruass» OUT NOW!
    image: assets/images/kaufmann_en_liaba_gruass_album_cover.jpg
    cta_link: https://linktr.ee/kaufmannmusik
    pressefoto: assets/pressefotos/Pressefoto_Kaufmann_04_RGB_credits_simon-garcia.jpg

shop:
  link: https://mercharsenal-europe.com/pages/kaufmann
  lp:
    image: assets/images/mockup_kaufmann_lp.png
    link: https://mercharsenal-europe.com/pages/kaufmann


musik: 
  spotify_widget: https://open.spotify.com/embed/playlist/46wsJwG1qFElvHi9HQX0kj?si=470d34c0dc87494f&amp;nd=1&amp;utm_source=oembed&theme=0

videos:
  - url: https://www.youtube.com/embed/QtsyoMyea2M # Heiweh (Live Session)
  - url: https://www.youtube.com/embed/pg_jqcF5Xyo # en liaba gruass
  - url: https://www.youtube.com/embed/Hm51bXuBH-c # 10'000 Floskla (Live Session mit Lyrics)
  - url: https://www.youtube.com/embed/i8d1__acpcc # Liachtigkeit Video
  - url: https://www.youtube.com/embed/85lnwqdAT4g # Heiweh Lyric Video
  - url: https://www.youtube.com/embed/Wmp_aAYXC38 # Schön Gsi Do
  - url: https://www.youtube.com/embed/DTAjLa5XaDQ # Blau
  - url: https://www.youtube.com/embed/Bf3xYFK0aU8 # Lisa
  - url: https://www.youtube.com/embed/COkSz6iVgzw # Norwegischi Chrona live @ OASG
  - url: https://www.youtube.com/embed/pYL1Qmaok0s # Pizza und Tinder

EPK:
  link: https://drive.google.com/drive/folders/1b1F29ds8gIZWn6LsvlAPt01qBX1bRCbh

contacts:
  - band:
    text: "Band"
    email: "band@kaufmannmusik.ch"
  - booking:
    text: "Booking"
    email: "noemi@lauter.ch"
  - management:
    text: "Management"
    email: "philippe@kaufmannmusik.ch"
  - label:
    text: "Label"
    email: "raphi@lauter.ch" 

socials:
  - facebook:
    icon_name: "fa fa-facebook"
    url: "https://www.facebook.com/kaufmannmusic"
  - spotify:
    icon_name: "fa fa-spotify"
    url: "https://open.spotify.com/artist/52Rw95lj0m3dqV8dqNS2Tw"
  - apple_music:
    icon_name: "fa fa-apple"
    url: "https://music.apple.com/ch/artist/kaufmann/1217160"
  - instagram:
    icon_name: "fa fa-instagram"
    url: "https://instagram.com/kaufmann_musik"
  - youtube:
    icon_name: "fa fa-youtube"
    url: "https://www.youtube.com/@kaufmannband"
  - tiktok:
    icon_name: "fa-brands fa-tiktok"
    url: "https://tiktok.com/@kaufmannmusik"

navigations:
  - home:
    url: "#home"
    text: Home
  - musik:
    url: "#musik"
    text: Musik
  - shows:
    url: "#shows"
    text: shows
  - shop:
    url: "#shop"
    text: Shop
#  - fotos:
#    url: "#fotos"
#    text: Fotos
  - videos:
    url: "#videos"
    text: Videos
  - kontakt:
    url: "#kontakt"
    text: Kontakt


---
